import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import gameTurn from './gameTurn';

const rootReducer = combineReducers({gameTurn, routing: routerReducer });

export default rootReducer;
