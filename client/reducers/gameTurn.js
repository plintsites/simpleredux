// a reducer takes in two things:

// 1. the action (info about what happened)
// 2. copy of current state

// gameTurn: state is just a boolean
function gameTurn(state = [], action) {
    switch (action.type) {
    	case 'CHANGE_TURN':
    		if (state) {
    			console.log('Pannie go deep');
    			return false;
    		}
    		console.log('Lim also');
    		return true;
    	default:
    		return state;
    }
}

export default gameTurn;
